#!/bin/sh

rm -r build-en
rm -r build-es
rm -r build-it
rm -r build-fi
rm -r build-sr
rm -r build-sr_Latn

sphinx-build -D language=en source/ build-en
sphinx-build -D language=es source/ build-es
sphinx-build -D language=it source/ build-it
sphinx-build -D language=fi source/ build-fi
sphinx-build -D language=sr source/ build-sr
sphinx-build -D language=sr_Latn source/ build-sr_Latn
