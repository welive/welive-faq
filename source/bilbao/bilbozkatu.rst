Bilbozkatu's FAQ
================

What is BILBOZKATU useful for?
------------------------------

If you want to improve your neighbourhood you can create a proposal which will be voted and commented by other citizens and thus the city hall can measure the local support of that proposal and act consecuently. It is a very good channel to communicate your ideas to the city hall. 
