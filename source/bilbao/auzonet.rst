Auzonet's FAQ
=============

What is AUZONET useful for?
---------------------------

Social network for neighborhoods in Bilbao for providing solutions to the inhabitants of those neighborhoods, based on the proximity and confidence.
Auzonet enables Bilbao's citizens to borrow the things they need from their neighbors in less than 30 minutes. Citizens may share their needs for other neighborhoods and publish the things they can offer to other people too. Citizens can search in others offers or find something to lend to a neighborhood that have published his/her need. 
Auzonet contributes to create "Community", putting in contact neighbors willing to lend things with citizens that borrow a specific thing.
You may find this application in Google Play: https://play.google.com/store/apps/details?id=com.tecnalia.auzonet
