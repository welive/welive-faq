==================
Osm-elements's FAQ
==================

What is OSM-ELEMENTS useful for?
-----------------------------------------------------

OSM-ELEMENTS is a service that extracts geoinformation data from OpenStreetMap (OSM) in an easy way. OSM is a collaborative project to create a free editable map of the world. To know more about OSM go to OSM_. OSM-ELEMENTS allows citizens to get information about the world, providing access to a huge database, which can be edited by themselves. The service is a tool to update,create and retrieve data from OSM (more information below) .

.. _OSM: http://www.openstreetmap.org/about 

Getting started
---------------------------

First of all we need to understand the structure of OSM. Each "data point" in OSM is called element. Each element can be of three types: node, way, relation. Also, every element must have some information, for example, changeset, the user who created the element, location or its timestamp. In addition, some elements may have tags which specify information such as, address, name, amenity...So, OSM's structure is something like:

                        -----------or---------- *Nodes* -> user, location,changeset, *tags**

*Elements*    ---are ---- *Ways* -> user, location,changeset, *tags**

                        -----------or---------- *Relations* -> user, location,changeset, *tags**

\* May have or not

You will find more information at OSMWiki_.

Osm-elements can retrieve all *elements* or just *nodes*, *ways* or *relations*. It also lets us filter this *elements*/*nodes*/*ways*/*relations* by tags. As we said, every element in OSM has a changeset_ number (id), which identifies a group of changes made by a single user over a short period of time. It is necessary to every change in OSM to provide a changeset id, so the workflow to use this service should be:

1. Create a changeset (*create/changeset*) and store its value.
2. Use service.
3. Close changeset (*delete/changeset*).

.. _OSMWiki: http://wiki.openstreetmap.org/wiki/Main_Page
.. _changeset: https://wiki.openstreetmap.org/wiki/Changeset

How to
---------------

**Selecting the area**

To get started we need to select the area from we will get our information. Our service needs to know the coordinates of that area, will use this tool_ to get those coordinates. This tool allows us to select an area, which we will call *bounding box*, from now on. Once we have the wanted *bounding box*,  we should go to the bottom side left side and select "CSV" under "Copy & Paste". Then we just need to save the coordinates wherever we want.

IMPORTANT: the bigger the area is, the longer will take the service to return the data.

.. _tool: http://wiki.openstreetmap.org/wiki/Main_Page

**Using the service**

With the area selected we can start using the service. Currently we have two ways to make use of our service:

1. Using swagger ui at osm-elements_.
2. Requesting directly our service (e.g. via web browser, curl...). 

In order to use the services that edit OSM we **need** to get token and token secret for signing our requests. In this moment, users of this service have to get both parameters using Oauth 1.0a and send them over requests headers. To get those tokens, consumer key and consumer secret from this service is needed:

* Consumer key: 6sRIrtOY6KoERUXvcajTisp52naNseltpX1FDzSW
* Consumer secret: MxiPm0a4vt9k1rs4hooNcIv7qjoLuBwQ1QmeuBys

We recommend to use Oauth 1.0a available libraries like java-library_ or python-library_ to get tokens. More information about oauth 1.0a and oauth in OSM can be found at: oauth-core_ and OSM-oauth_.

.. _osm-elements: http://osm-elements.cloudfoundry.welive.eu/
.. _OSM-oauth: http://wiki.openstreetmap.org/wiki/OAuth 
.. _oauth-core: https://oauth.net/core/1.0a/ 
.. _java-library: https://github.com/mttkay/signpost
.. _python-library: https://github.com/litl/rauth

**1. Swagger ui**

Once we've accessed osm-elements_, we will find the API of the service. As you see there are different services sorted by their function. For example, *create* service contains those that create elements in OSM, *ways* service allows us to retrieve elements of type way in several ways. Services that need authorization (basically those that edit anything) ask for *token* and *tokenSecret*.

For example lets see *nodes* service, clicking on it we will see every operation and a brief description of each of them. Now we need to select the operation we want to make use of. *POST* operations require a body parameter that has to be filled with the JSON model observable in the right side. For the *nodes* service all of those models contains coordinates corresponding to the ones we saved in *Using the service* section. Additionally, we might have to add a parameter to the JSON:

- Name: write down the name of the element you're looking for.
- Amenity: it specifies the type of amenity we're looking for. Possible amenities are listed at OSM-amenity_.
- Shop: it specifies the type of shop we're looking for. Possible shop types are listed at OSM-shop_.

For instance let’s suppose we want to get all the banks in London. We have already saved our bounding box coordinates of London (-0.194321,51.48044,-0.063858,51.528485). So we should go to the *nodes* service and in the body of *nodes/amenity* we should write:

{ 

"coordinates": { 

"topLeft": "-0.194321",

"topRight": "51.48044",

"bottomLeft": "-0.063858",

"bottomRight": "51.528485"

},

"amenity": "cafe"

}

Finally we have to click on "Try it out!" and it will return the data in the response body textbox.

*GET* services are easier to use, we just need to fill the parameters we are asked for and click on "Try it out!".

.. _OSM-amenity: http://wiki.openstreetmap.org/wiki/Key:amenity
.. _OSM-shop: http://wiki.openstreetmap.org/wiki/Key:shop


**2. Direct request**

Direct request revolves around calling the url of the wanted service operation. It might be done using different tools such as web browsers, curl, code http requests... Anything to make an http request.
We will use curl_ as an example.

We will try to create a new node in Bilbao, to be exact we will create the University of Deusto (UD). First of all, we need to loke the API for the *create/node* service. We will see that currently it asks for a *token* and *tokenSecret* as header parameters and a body with this model:

{

  "changeset_id": "string",

  "latitude": "string",

  "longitude": "string",

  "name": "string",

  "amenity": "string",

  "address": "string",

  "website": "string",

  "telephone": "string",

  "opening_hours": "string"

} 


We will use a *token* and a *tokenSecret* we already requested. Then we also need the latitude and longitude coordinates of UD, for that we shall use another service called geoutilities_. We know (otherwise we can search) that the address of UD is "Unibertsitatearen Etorbidea", using geoutilities we can get the latitude and longitude coordinates. We already have everything we need know so let's do it, but remember first we need to open a changeset:

1. curl -X GET --header 'Accept: text/plain' --header 'token: 8tAHXmVUkuQZsCAlcu1O0IoM3cqlBtbmNFe6gSKh' --header 'tokenSecret: FsT85RoylJfw4i7kZjf2dsJtxCzLd0Eh6EeMZgnQ' 'http://osm-elements.cloudfoundry.welive.eu/api/create/changeset'

We should see a response like: {"id":"46518001"}, now we need to save that id number and put it the changeset id field of the next request's body.

2. curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' --header 'token: 8tAHXmVUkuQZsCAlcu1O0IoM3cqlBtbmNFe6gSKh' --header 'tokenSecret: FsT85RoylJfw4i7kZjf2dsJtxCzLd0Eh6EeMZgnQ' -d '{ \ 
   "changeset_id": "46517993", \ 
   "latitude": "43.2712452", \ 
   "longitude": "-2.9385575999999674", \ 
   "name": "Universidad de Deusto", \ 
   "amenity": "university", \ 
   "address": "Unibertsitatearen etorbidea", \ 
   "website": "www.deusto.es", \ 
   "telephone": "string", \ 
   "opening_hours": "string" \ 

 }' 'http://osm-elements.cloudfoundry.welive.eu/api/create/node'

Now we get as response the id number of the node we created.

3. Finally, because we won't edit anything else we close our changeset:

curl -X GET --header 'Accept: text/plain' --header 'token: 8tAHXmVUkuQZsCAlcu1O0IoM3cqlBtbmNFe6gSKh' --header 'tokenSecret: FsT85RoylJfw4i7kZjf2dsJtxCzLd0Eh6EeMZgnQ' 'http://osm-elements.cloudfoundry.welive.eu/api/delete/changeset?changesetId=46517993' 
 |
 
As response "Changeset closed"

.. _curl: https://curl.haxx.se/ 
.. _geoutilities: http://geoutilities.cloudfoundry.welive.eu/





