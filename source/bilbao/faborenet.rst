Faborenet's FAQ
===============

What is FABOREnet useful for?
-----------------------------

Social network for neighborhoods in Bilbao for providing solutions to the inhabitants of those neighborhoods, based on the proximity and confidence.
Faborenet enables Bilbao's citizens to publish an activity/task needed. A neighbor can see the task and make an offer to do it. Citizens can put their needs for other neighborhoods and put the task they can offer to do to another people too. Citizens may also search in others offers or find a task they can do to a neighborhood that has published his/her need. 
