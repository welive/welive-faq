BilbOn's FAQ
============

What is BILBON useful for?
--------------------------

If you are a visitor or citizen in Bilbao looking for an specific public service (bike sharing, restaurant, hotel, public information centre...) you can use Bilbon to locate these services. Bilbon uses different dataset and shows that information on a map. Also users can contribute on the creating of these POI by uploading their own information.
