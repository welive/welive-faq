Safe City Trip's FaQ
====================

What is SAFE CITY TRIP useful for?
----------------------------------

Safe city trip is both web and android service that allows 
citizens to make optimal city trip planning based on their 
specific needs. Based on the information about pollen
(to be implemented), traffic congestion and quality of 
surface water, they will get the optimal route to safely go 
by bicycle and enjoy in swimming and outdoor
activities. Furthermore, they will be able to avoid 
unexpected roadwork and congested streets.

