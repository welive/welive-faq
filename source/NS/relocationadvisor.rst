Relocation Advisor's FaQ
========================

What is RELOCATION ADVISOR useful for?
--------------------------------------

Relocation Advisor is both web and android service
which allows citizens and business people to find an 
appropriate apartment based on open data-information 
on public utility infrastructure in relation to their 
residential purposes, citizens are able to get data 
about vicinity of parks, playgrounds, kindergartens, 
schools, as well as traffic congestion, noise, and 
other information related to environment and 
neighborhood of their place of interest. This 
information will give better insight about considered 
city area for living purposes.


