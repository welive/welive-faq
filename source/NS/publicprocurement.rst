Public Procurement's FaQ
========================

What is PUBLIC PROCUREMENT TRANSAPRENCY useful for?
---------------------------------------------------

Public procurement transparency & Public procurement 
follower is the service that allows public administration and 
citizens to follow Public procurement process. This service
allows citizens and companies to receive clear information 
about new competitions that they could apply to, accompanied 
with the possibility to obtain information on companies from 
the same activity, the concluded agreements and similar.  
For public administration persons, it will allow perceiving 
weaknesses and advantages of applying the Law in practice, 
giving the possibility of simplifying the procedures and steps 
and having the initiative to amend the Law on Public 
Procurement. Also, based on self-government public 
procurement, open data can be used for making various
studies and analysis of importance for the City.

