My Neighbourhood's FAQ
======================

What is My Neighbourhood useful for?
------------------------------------

My Neighborhood application helps users to find city regions that would match their personal preferences. Match scores are based on various open datasets combined with citizen opinions submitted with the My Opinion application.
