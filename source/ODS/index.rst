Datasets
========

What is the difference between a dataset and a resource?
--------------------------------------------------------

A dataset represents a data package formed by one or more data resources. A dataset contains two main fields:

* Metadata about the data represented by the dataset.
* Resources containing the data itself.

You can find more information about datasets and resources in `WeLive Documentation <https://dev.welive.eu/documentation/user-guide/ods/dataset.html>`_.

How can I map a resource for querying with the Query Mapper?
------------------------------------------------------------

If the "Mapping" field from the resource editing form is leaved empty, the Open Data Stack tries to generate an automatic mapping for this resource. From this automatic mapping, user can modify this mapping to fit to her needs.

You can find more information about how to map datasources in `WeLive Documentation <https://dev.welive.eu/documentation/user-guide/ods/query-mapper/mapping.html>`_.

How can I create a dataset to be accessed from an application?
--------------------------------------------------------------

For creating a dataset to be accessed from an app, you must fill the mapping field from the resource you want to access from the app. When leaving this field empty, the ODS tries to generate a default mapping. If this mapping does not fit your needs, you can check `ODS's documentation <https://dev.welive.eu/documentation/user-guide/ods/index.html>`_ for creating a custom mapping.
