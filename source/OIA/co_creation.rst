Co-creation
===========

What is a co-creation process?
------------------------------

The service co-creation in WeLive is a process that allows actors involved (citizens, companies, academies and municipalities) to collaborate with each other to improve their city by creating together useful public services, data and open processes.

How can I collaborate and communicate with other users?
--------------------------------------------------------

A Social section allows you to keep contacts with other users by creating connections with them and by sending and receiving messages. To collaborate with other users, you can:
 * choose them as collaborators of your ideas according to their skills and their reputation score;
 * vote or leave comments in their ideas and needs. 
