Challenges
==========

What is a challenge?
--------------------

A challenge is an open call launched by the Municipality starting from one or more needs that allows registered users to suggest ideas in order to solve the proposed problem. 

How can I create a challenge?
-----------------------------

The challenge can be created only by the Municipality. If you are a Municipality representative click on:

https://dev.welive.eu/documentation/user-guide/oia/logged-in-user-authority.html#creating-a-challenge

