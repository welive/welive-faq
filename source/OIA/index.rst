Open Innovation Area's FAQ
==========================

In this page, frequently asked questions about the Open Innovation Area (OIA) are shown.


What is the Open Innovation Area (OIA)?
---------------------------------------

The Open Innovation Area (OIA) component is dedicated to boost the level of collaborative research and development as a mechanism to fuel innovative discovery of novel public services.

It enables citizens to express their needs. Public administrations raise challenges as result. Citizens to pose ideas to address the challenges and vote for those more appealing ideas. Councils to select the most demanded ideas so that real apps can be created.
