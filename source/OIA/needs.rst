Needs
=====

What is a need?
---------------

A need is an emerging or existing problem of public interest that a registered user (citizen, company or academy) can describe into the platform and can point out to the Municipality to solve it. 

How can I create a need?
------------------------

In order to create a need, you can view the user guide: 

https://dev.welive.eu/documentation/user-guide/oia/logged-in-user-citizen.html#creating-a-need-report

Can I solve a need submitted by another citizen?
------------------------------------------------

Yes, you can solve a need submitted by another citizen by proposing an idea that addresses the problem expressed in the need. 

Who will take in charge my need?
---------------------------------

The Municipality could take in charge your need by launching a challenge that addresses the need.

