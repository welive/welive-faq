Ideas
=====

What is an idea?
----------------

An idea is a possible solution that a registered user (citizen, company or academy) can propose for a need (created from other users) or for a challenge (launched by the Municipality).

How can I create an idea?
-------------------------

In order to create an idea, you can view the user guide:

https://dev.welive.eu/documentation/user-guide/oia/logged-in-user-citizen.html#creating-an-idea

How will the ideas be evaluated for a challenge?
------------------------------------------------

Each idea will be evaluated based on a set of quantitative and barrier criteria, chosen by the Municipality, together with an experts team. Each idea will have a total score given by the sum of all weighted scores.
READ MORE: 

How will the experts team be appointed?
---------------------------------------

Experts can be internal or external to the platform. They are chosen by the Municipality according to the skills required for the challenge.
READ MORE:

How many ideas for each challenge can be selected to be refined?
----------------------------------------------------------------

It is up to the Municipality to decide the number of ideas to be promoted. If the Municipality provides a reward for the challenge, it will also provide the number of ideas during the challenge creation process. Else, this information is not mandatory.
READ MORE: 

How can I implement my idea?
----------------------------

Your idea can be implemented with the help of the collaborators that you have chosen for your idea and through WeLive tools (such as Marketplace and Visual Composer) that allow you to reuse and compose pre-existing elements, useful to implement your idea. 
