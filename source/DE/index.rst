Decision Engine's FAQ
==========================

In this page, frequently asked questions about the Decision Engine (DE) are shown.

What is the Decision Engine?
----------------------------

The WeLive Decision Engine is a component which enables to bring together the final user interests, needs and wants with the ecosystem of public services (apps) made available in the WeLive Marketplace. Combined
with the Citizen Data Vault allows matching user profiles with the metadata of available WeLive artifacts, i.e. datasets, building blocks and apps, in order to provide the best fitting ones to users. The interaction of WeLive
Decision Engine, upon request from either the WeLive Controller UI or the WeLive Player, with the WeLive Marketplace permits to suggest the best services fitting the user profile and current context, e.g. current
location. Internally, the WeLive Decision Engine is configured as a range of recommenders which retrieving metadata from CDV and Marketplace can crosslink WeLive users with artifacts. Concretely, the following
recommenders are available: a) artifacts vs. artifacts, b) users vs. artifacts, c) ideas vs. ideas and d) ideas vs. collaborators. It is planned to extend the capabilities of the Decision Engine to also enable ideas vs. collaborators recommendations. 
