Platform Usage: User's and programmer's perspective
===================================================

What can you do through WeLive user interface?
----------------------------------------------

The WeLive welcome (http://dev.welive.eu) page gives you the possibility to access to an ecosystem of IT tools. By means of these tools you have the chance to:

* Propose and Idea and vote: The Open Innovation area is an open space where you may express your ideas and even vote for them.
* Get your idea done: The Visual Composer and the Open Data Stack enable you to co-create new applications by combining several building blocks built on top of the Open Data model of your city.
* Share your app with others!: The marketplace is the place where all the apps, building blocks and datasets may be browsed, selected and purchased.
* Find new apps and enjoy your city!: The "WeLive Player" mobile app will allow you to access to the apps related to your city anywhere and anytime.
* Monitor how your city is performing: The Analytics Dashboard allows the public administration to see how citizens are interacting with the WeLive platform tools and to compare your city with other WeLive cities.

You can find more information about datasets and resources in `WeLive Documentation <https://dev.welive.eu/documentation/user-guide/ods/dataset.html>`_.

How is programmatic access to WeLive enabled (RESTful API)?
-----------------------------------------------------------

The WeLive project's Rest API uses Swagger. Swagger is a simple yet powerful representation of your RESTful API.  With a Swagger-enabled API, you get interactive documentation, client SDK generation and discoverability. The access to the Swagger interface is allocated in https://dev.welive.eu/dev/swagger/

The Rest API documentation is available in https://dev.welive.eu/documentation/api-guide/index.html

How can I publish an artefact in Marketplace
---------------------------------------------

What restrictions has to fulfil to be published in WeLive?
----------------------------------------------------------