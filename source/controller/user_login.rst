User management
===============

How can I sign up into WeLive Platform?
---------------------------------------

Browse to https://dev.welive.eu. Click on the "Sign In" button in the upper-right corner. Click on the text "Don't have a WeLive account? Register here". Read and accept the "Terms of Use " by cliking on the "I agree" button at the end of the page. Complete the registration form providing all the required information and possibly some optional information that might improve your experience with the WeLive framework. Click on the "Save" button. You will receive an email with the registration confirmation and a password to sign in.
  

How can I sign in into WeLive Platform?
---------------------------------------

You can sign in using a WeLive account that has been registered. You can also use an existing Google or Facebook account. In this case, at the first access you will be required to complete a registration form. Complete the registration form providing all the required information and possibly some optional information that might improve your experience with the WeLive framework.