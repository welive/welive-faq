# Instructions for translating the WeLive Project FAQ

* Requirements:
    * Python 2.7
    * virtualenv


* Create and activate the virtual environment:



```
#!bash

    $ virtualenv welive-faq
    $ cd welive-faq
    $ source bin/activate
    $ git clone https://bitbucket.org/welive/welive-faq
    $ pip install -r welive-faq/requirements.txt
    $ cd welive-faq
```


* Write questions and answers in **English** in the correspondent \*.rst file. Each component has its own folder. Apps and BB from municipalities have their own folder too.

* Compile the FAQ and ensure that there are no errors.



```
#!bash

    $ make html

```

* Extract labels for the new generated documentation.



```
    $ make gettext

```

* Execute the following commands for generating translation files for other languages.



```
#!bash

    $ sphinx-intl update -p build/locale/

```

* Translate the correspondent \*.po files for your language under `source/locale/<language_code>/LC_MESSAGES` folder. You can check examples in Spanish folder.

* Commit and push changes.

* Notify to welive-all that new questions and answers have been published, to ensure that members from other task forces translate them to their language.

* UDEUSTO will be in charge of compiling and deploying the FAQ in different languages.
